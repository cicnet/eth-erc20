Hello and welcome to the CIC Stack repository. Targeted for use with the ethereum virtual machine and a ussd capable telecom provider. 

__To request a change to the code please fork this repository and sumbit a merge request.__

__If there is a Grassroots Economics Kanban Issue please include that in our MR it will help us track contributions. Karibu sana!__

__Visit the Development Kanban board here: https://gitlab.com/grassrootseconomics/cic-internal-integration/-/boards/2419764__

__Ask a question in our dev chat:__

[Mattermost](https://chat.grassrootseconomics.net/cic/channels/dev)

[Discord](https://discord.gg/XWunwAsX)

[Matrix, IRC soon?]
